provider "google" {
    credentials = file(var.gcp_credentials_path)
    project = var.project_name
    region  = var.region
    zone    = var.zone
}

# prod instance
resource "google_compute_instance" "production"{
    # for scaling if needed
    count= var.instance_num
    name= "production-${count.index + 1}"
    machine_type= "e2-medium"
    labels = {
        "server"="",
        "production"="",
        "webapps"=""
    }
    boot_disk {
        initialize_params {
        image = "ubuntu-os-cloud/ubuntu-2004-lts"
        }
    }
    # metadata = {
    #     ssh-key= "${var.ssh_username}:${file(var.ssh_pub_key_path)}"
    # }
    network_interface {
    # A default network is created for all GCP projects
        network = "default"
        # assign static ip
        access_config {
            nat_ip= google_compute_address.stat.address
        }
    }
    # user default http and https firewall
    tags = ["http-server", "https-server"]
}

# stag instance
resource "google_compute_instance" "stag"{
    # for scaling if needed
    count= var.instance_num_stag
    name= "staging-${count.index + 1}"
    machine_type= "e2-medium"
    labels = {
        "server"="",
        "staging"="",
        "webapps"=""
    }
    boot_disk {
        initialize_params {
        image = "ubuntu-os-cloud/ubuntu-2004-lts"
        }
    }
    # metadata = {
    #     ssh-key= "${var.ssh_username}:${file(var.ssh_pub_key_path)}"
    # }
    network_interface {
    # A default network is created for all GCP projects
        network = "default"
        # assign static ip
        access_config {
        }
    }
    # user default http and https firewall
    tags = ["http-server", "https-server"]
}

resource "google_compute_firewall" "http"{
    name    = "http-server"
    network = "default"
    allow {
        protocol = "icmp"
    }
    # open http port
    allow {
        protocol = "tcp"
        ports    = ["80"]
    }
    # subnet internal connection
    source_ranges = ["0.0.0.0/0"]
    
    target_tags = ["http-server"]
}

resource "google_compute_firewall" "https"{
    name    = "https-server"
    network = "default"
    allow {
        protocol = "icmp"
    }
    # open https port
    allow {
        protocol = "tcp"
        ports    = ["443"]
    }
    # subnet internal connection
    source_ranges = ["0.0.0.0/0"]
    
    target_tags = ["https-server"]
}

# set static IPv4 addr for prod vm
resource "google_compute_address" "stat" {
    name = "ip4-addr"
}
